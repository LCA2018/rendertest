FROM python:3.6 as badger_base

VOLUME /app/render

RUN set -ex \
    && apt-get update

COPY pkgs /tmp/pkgs
RUN set -ex \
    && pkgs=$(cat /tmp/pkgs) \
    && apt install -y $pkgs

COPY fonts/*.ttf /usr/local/share/fonts/
RUN set -ex \
    && fc-cache \
    && fc-list

COPY requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt

WORKDIR /app/render
CMD ["svg42pdf", "-m", "imagemagick", "badge.svg", "badge.pdf"]
